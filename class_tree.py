#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jan 27 2021
"""

class Tree:
    """
    This class makes it possible to define methods of insertions,
    deletions that can be carried out on the binary tree.
    """

    def __init__(self, root_node):
        """
        This function initialises our tree with the root/parent node.

        Parameters
        ----------
        root_node : string
        """
        self.root_node = root_node


    def traversal_deep(self):
        """
        Function that allows the visualization of the tree in its depth
        According to the display format in the str function
        of the TreeNode class
        """
        print(self.root_node)


    def insert(self, added_node, target_node):
        """
        Function for declaring the insertion of a node in the binary tree

        Parameters
        ----------
        added_node : the new node to be inserted
        target_node : the desired node on which the insertion is made
        """
        # If our tree doesn't have a root, we insert the new knot as such
        if self.root_node is None:
            root_node = added_node
        # If the node you are looking for is not a leaf (it has children)
        elif not target_node.if_leaf():
            # But no child on the left
            if target_node.left_child is None:
                # Insert the new node here
                target_node.left_child = added_node
                # as the left child of the target node
            # But no child on the right
            elif target_node.right_child is None:
                # Insert the new node here
                target_node.right_child = added_node
                # as the right child of the target node
            else: # Indeed, on "else", we cannot set conditions
                self.insert(added_node, target_node.left_child)
                # In any case the new node is by default on the
                # left side of the target node


    def delete_leaf(self, target_node):
        """
        Deletion function of a node if it's a leaf
        (that it has no children)

        Parameters
        ----------
        target_node : the desired node on which the deletion is made
        """
        if target_node.if_leaf:
            target_node = None
        else:
            return


    def delete_node(self, target_node):
        """
        Deletion function of a selected node

        Parameters
        ----------
        target_node : the desired node on which the deletion is made
        """
        # If the node chooses to delete is the root
        if target_node is self.root_node:
            print("We can't remove the root of the tree !")
            return
        # If the node chooses to delete is childless
        if target_node.if_leaf:
            self.delete_leaf(target_node)
        else: # Otherwise, if he has a right child
            if target_node.right_child is not None:
                target_node.data = target_node.right_child
                target_node.right_child = None
            else: # Otherwise, if he has a left child
                target_node.data = target_node.left_child
                target_node.left_child = None
