#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jan 27 2021
"""

from __future__ import absolute_import

# Import of the files containing the classes, defining the functions
# for manipulating the nodes of the binary tree
from class_tree_node import TreeNode
from class_tree import Tree


if __name__ == "__main__":
    n0 = TreeNode('INLO')
    n1 = TreeNode('TD')
    n2 = TreeNode('Cours')
    n0.right_child = n1
    n0.left_child = n2

    # Visualization of the entire tree
    n0.print_tree_node()

    # Returns the boolean of the if_leaf function
    # to the position at the end of the tree
    print(n0.if_leaf())
    print(n1.if_leaf())

    # Depth visualization of the tree.
    # Visualization of the tree, links between nodes (kinship relationships)
    root = TreeNode('INLO')
    c_tree = Tree(root)
    c_tree.insert('TD_1', n1)
    c_tree.traversal_deep()
