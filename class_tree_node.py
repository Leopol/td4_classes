#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jan 27 2021
"""

class TreeNode:
    """
    This class is used to define methods for constructing and displaying
    the binary tree, as well as certain parameters (if_leaf)
    """

    def __init__(self, data):
        """
        Function for initializing the parameters of a node

        Parameters
        ----------
        self.data : the root node, the data of a node, its nomination (string)
        self.right_child : the right son node (string)
        self.left_child : the left son node (string)
        """
        self.data = data
        self.right_child = None
        self.left_child = None


    def __str__(self):
        """
        Function indicating the display format of the binary tree
        """
        if self.if_leaf():
            str(self.data)
        else:
            display = "["+ str(self.left_child) +";"+ str(self.right_child) +"]:"+ str(self.data)
            print(display)


    def print_tree_node(self):
        """
        Displays the entire binary tree :
        Construction of a part of the tree by associating
        a parent node and these two children

        Parameters
        ----------
        self.data : the root node
        self.right_child : the right son node
        self.left_child : the left son node
        """
        # "Root" node display
        print(self.data)
        # Right node display
        if self.right_child:
            self.right_child.print_tree_node()
        # Left node display
        if self.left_child:
            self.left_child.print_tree_node()


    def if_leaf(self):
        """
        This function allows us to return a boolean if the node is a leaf of the tree,
        that we find it at the end, their links are defined by None.
        """
        return self.right_child is None and self.left_child is None
